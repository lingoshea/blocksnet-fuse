#include <bsnfs/bsnfs.h>
#include <tools/arch/bsnfs_error.h>
#include <ctime>
#include <cstring>

#define MIN(a, b) ((a) < (b) ? (a) : (b))

bsnfs_block_t bsnfs::find_in_dir(const char * name, bsnfs_block_t dir_block, bsnfs_block_map_t & block_map)
{
    bsnfs_dir_head_t dir_head{};
    block_map.read((char*)&dir_head, sizeof(dir_head), 0, dir_block);
    // the old fashion way
    if (dir_head.dir_block_count == 1)
    {
        for (uint64_t i = 1; i <= dir_head.stat.st_size / sizeof(bsnfs_dentry_t); i++)
        {
            bsnfs_dentry_t dentry{};
            block_map.read((char*)&dentry, sizeof(dentry), sizeof(dentry) * i, dir_block);
            if (!strcmp(dentry.entry_name, name))
            {
                return dentry.dentry_block_num;
            }
        }
    }

    throw bsnfs_error_t(BSNFS_NO_SUCH_FILE_OR_DIRECTORY);
}

bsnfs_block_t bsnfs::nami(const bsnfs_pathname_t & pathname, bsnfs_block_map_t & block_map)
{
    try
    {
        bsnfs_block_t block = 1; // root dir

        if (pathname.get_pathname().size() == 1 && pathname.get_pathname()[0].empty())
        {
            return 1; // root
        }

        for (auto & entry_name : pathname.get_pathname())
        {
            block = bsnfs::find_in_dir(entry_name.c_str(), block, block_map);
        }

        return block;
    }
    catch (...)
    {
        throw;
    }
}

void bsnfs_block_map_t::read(char *buffer, bsnfs_block_t block_num)
{
    // TODO: This implementation is narrowing filesystem size
    device->seekg((std::streamoff)block_num * (std::streamoff)fs_head.fs_block_size * 1024,
                  std::ios::beg);
    device->read(buffer, (std::streamoff)fs_head.fs_block_size * 1024);
}

void bsnfs_block_map_t::write(char *buffer, bsnfs_block_t block_num)
{
    // TODO: This implementation is narrowing filesystem size
    device->seekp((std::streamoff)block_num * (std::streamoff)fs_head.fs_block_size * 1024,
                  std::ios::beg);
    device->write(buffer, (std::streamoff)fs_head.fs_block_size * 1024);
}

bsnfs_size_t bsnfs_block_map_t::read(char *buffer, bsnfs_size_t size, bsnfs_size_t offset, bsnfs_block_t block_num)
{
    bsnfs_size_t real_size = MIN(size, fs_head.fs_block_size * 1024);

    // TODO: This implementation is narrowing filesystem size
    device->seekg((std::streamoff)block_num * (std::streamoff)fs_head.fs_block_size * 1024 + (std::streamoff)offset,
                  std::ios::beg);
    device->read(buffer, (std::streamoff)real_size);
    return real_size;
}

bsnfs_size_t bsnfs_block_map_t::write(char *buffer, bsnfs_size_t size, bsnfs_size_t offset, bsnfs_block_t block_num)
{
    bsnfs_size_t real_size = MIN(size, fs_head.fs_block_size * 1024);

    // TODO: This implementation is narrowing filesystem size
    device->seekp((std::streamoff)block_num * (std::streamoff)fs_head.fs_block_size * 1024 + (std::streamoff)offset,
                  std::ios::beg);
    device->write(buffer, (std::streamoff)real_size);
    return real_size;
}

bsnfs_block_map_t::bsnfs_block_map_t(const std::fstream &_device)
: device(const_cast<std::fstream *>(&_device))
{
    device->seekg(std::ios::beg);
    device->read((char*)&fs_head, sizeof(fs_head)); // !! only direct I/O allowed !!

    if (fs_head.magic != BLOCKSNET_FILESYSTEM_MAGIC_NUMBER
    || fs_head.magic_comp != ~BLOCKSNET_FILESYSTEM_MAGIC_NUMBER)
    {
        throw bsnfs_error_t(BSNFS_FILESYSTEM_NOT_RECOGNIZED);
    }

    fs_head.if_mounted = 1;
    fs_head.last_cmount_time = std::time(nullptr);
    device->write((char*)&fs_head, sizeof(fs_head)); // !! only direct I/O allowed !!
}

