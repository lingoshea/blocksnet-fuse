#ifndef BLOCKSNET_FUSE_BSNFS_ERROR_H
#define BLOCKSNET_FUSE_BSNFS_ERROR_H

#include <string>
#include <utility>
#include <cstring>

#define BSNFS_FILESYSTEM_NOT_RECOGNIZED     0xA001  /* Filesystem not recognized */

#define BSNFS_NO_SUCH_FILE_OR_DIRECTORY     0xD001  /* No such file or directory */

/// Filesystem Error
class bsnfs_error_t : public std::exception
{
private:
    unsigned int error_code;
    error_t _errno;

public:
    /// Generate a error with error code
    /** @param _code Your error code **/
    explicit bsnfs_error_t(unsigned int _code) noexcept : error_code(_code), _errno(errno) {}

    /// Return explanation of current error
    [[nodiscard]] const char * what() const noexcept override;

    /// Return the explanation of errno snapshoted when the current error is generated
    [[nodiscard]] const char * what_errno() const noexcept { return strerror(_errno); };

    /// Return the errno snashoted when the current error is generated
    [[nodiscard]] error_t my_errno() const noexcept { return _errno; }

    /// Return the error code of current error
    [[nodiscard]] unsigned int my_errcode() const noexcept { return error_code; }
};

#endif //BLOCKSNET_FUSE_BSNFS_ERROR_H
