#include <bsnfs/bsnfs.h>
#include <cctype>
#include <getopt.h>
#include <iostream>
#include <tools/arch/bsnfs_error.h>
#include <fstream>
#include <filesystem>
#include <sys/stat.h>

/// Show help message
/** @param progname Program name, given by *argv **/
void help(const char * progname)
{
    printf("%s [OPTION] [DEVICE]\n"
           "\t--blksize   Specify block size (in Kb)\n"
           "\t--help      Show this help message\n",
           progname);
}

/// Format device
/** @param blksize  Block size
 *  @param pathname device
 *  @return true if succeeded, false if failed **/
bool format_device(bsnfs_size_t blksize, const char * pathname)
{
    std::fstream device(pathname, std::ios::in | std::ios::out | std::ios::binary);

    if (!device)
    {
        std::cerr << "Cannot open device `" << pathname << "' (errno=" << strerror(errno) << ")" << std::endl;
        return false;
    }

    ////////////////////////////////////////////////
    ////////////////// write head //////////////////
    ////////////////////////////////////////////////
    device.seekp(std::ios::beg);
    bsnfs_size_t device_block_count = std::filesystem::file_size(pathname) / (blksize * 1024);

    if (device_block_count < 4 || blksize < 4)
    {
        std::cerr << "Device is too small!" << std::endl;
        return false;
    }

    bsnfs_head_t filesystem_head =
            {
            .magic = BLOCKSNET_FILESYSTEM_MAGIC_NUMBER,
            .fs_block_size = blksize,
            .fs_block_count = device_block_count,
            .last_cmount_time = 0,
            .if_mounted = 0,
            .fs_current_mode = BSNFS_MODE_MAINTENANCE,
            .magic_comp = ~BLOCKSNET_FILESYSTEM_MAGIC_NUMBER,
            };

    device.write((char*)&filesystem_head, sizeof (filesystem_head));

    ////////////////////////////////////////////////
    ////////////////// create root /////////////////
    ////////////////////////////////////////////////

    timespec ts{};
    timespec_get(&ts, TIME_UTC);



    bsnfs_dir_head_t root =
            {
            .stat = {
                    .st_nlink = 2,
                    .st_mode = S_IFDIR | 0755,
                    .st_atim = ts,
                    .st_mtim = ts,
                    .st_ctim = ts,
                    },
            .dir_block_count = 1,
            };

    device.seekp((off_t)filesystem_head.fs_block_size * 1024);
    device.write((char*)&root, sizeof(root));

    device.close();
    return true;
}

int main(int argc, char ** argv)
{
    int option;
    bsnfs_size_t blksize = 4;

    while (true)
    {
        int option_index = 0;
        static struct option long_options[] = {
                {"blksize",         required_argument,  nullptr,    0 },
                {"help",            no_argument,        nullptr,    0 },
                {nullptr,           0,                  nullptr,    0 },
        };

        option = getopt_long(argc, argv, "b:h", long_options, &option_index);

        if (option == -1)
        {
            break;
        }

        switch (option) {
            case 0:
                switch (option_index)
                {
                    case 0: // blksize
                        blksize = strtoll(optarg, nullptr, 10);
                        break;

                    case 1:
                        help(*argv);
                        return 0;

                    default:
                        return 1;
                }

            case 'b':
                blksize = strtoll(optarg, nullptr, 10);
                break;

            case 'h':
                help(*argv);
                return 0;

            case '?':
                if (optopt == 'b')
                {
                    fprintf(stderr, "Option -%c requires an argument.\n", optopt);
                }
                else if (isprint (optopt))
                {
                    fprintf(stderr, "Unknown option `-%c'.\n", optopt);
                }

                return 1;

            default:
                return 1;
        }
    }

    if (optind == argc)
    {
        std::cerr << "No device specified!" << std::endl;
        return 1;
    }

    int ret = 0;

    while (optind < argc)
    {
        try
        {
            if (!format_device(blksize, argv[optind]))
            {
                std::cerr << "Formatting device `" << argv[optind] << "' FAILED!" << std::endl;
                optind++;
                ret = 1;
                continue;
            }

            std::cout << "Device `" << argv[optind] << "' formatted!" << std::endl;
            optind++;
        }
        catch (bsnfs_error_t & err)
        {
            std::cerr << "ERROR: " << err.what() << " (errno=" << err.what_errno() << ")" << std::endl;
        }
    }

    return ret;
}
