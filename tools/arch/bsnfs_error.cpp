#include <tools/arch/bsnfs_error.h>

const char *bsnfs_error_t::what() const noexcept
{
    switch (error_code)
    {
        case BSNFS_NO_SUCH_FILE_OR_DIRECTORY:
            return "No such file or directory";

        default:
            return "Unknown";
    }
}
