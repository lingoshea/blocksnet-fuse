#ifndef BLOCKSNET_FUSE_DO_FUSE_H
#define BLOCKSNET_FUSE_DO_FUSE_H

#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <bsnfs/bsnfs.h>

extern bsnfs_block_map_t * block_map;

extern
struct fuse_operations operations;

static
int bsnfs_do_readdir (const char *, void *, fuse_fill_dir_t, off_t, struct fuse_file_info *);

static
int bsnfs_do_getattr (const char *, struct stat *);

#endif //BLOCKSNET_FUSE_DO_FUSE_H
