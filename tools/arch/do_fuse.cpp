#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <sys/types.h>
#include <iostream>
#include <tools/arch/bsnfs_entry.h>
#include <bsnfs/bsnfs.h>
#include <tools/arch/bsnfs_error.h>

// output error
#define OUTPUT_ERROR(error) std::cerr << (error).what() << " (errno=" << (error).what_errno() << ")" << std::endl

bsnfs_block_map_t * block_map = nullptr;

static
int bsnfs_do_readdir (const char * c_pathname, void * buffer, fuse_fill_dir_t filler, off_t offset,
                      struct fuse_file_info * fi)
{
    try
    {
        bsnfs_pathname_t pathname(c_pathname);
        auto dir_block = bsnfs::nami(pathname, *block_map);

        filler( buffer, ".", nullptr, 0 );
        filler( buffer, "..", nullptr, 0 );

        bsnfs_dir_head_t dir_head{};
        block_map->read((char*)&dir_head, sizeof(dir_head), 0, dir_block);
        // the old fashion way
        if (dir_head.dir_block_count == 1)
        {
            for (uint64_t i = 1; i <= dir_head.stat.st_size / sizeof(bsnfs_dentry_t); i++)
            {
                bsnfs_dentry_t dentry{};
                block_map->read((char*)&dentry, sizeof(dentry), sizeof(dentry) * i, dir_block);
            }
        }

        return 0;
    }
    catch (bsnfs_error_t & error)
    {
        OUTPUT_ERROR(error);
        return -error.my_errno();
    }
    catch (...)
    {
        return -1;
    }
}

static
int bsnfs_do_getattr(const char * c_pathname, struct stat * st)
{
    try
    {
        bsnfs_pathname_t pathname(c_pathname);
        bsnfs_inode_t inode{};
        auto inode_block = bsnfs::nami(pathname, *block_map);
        block_map->read((char*)&inode, sizeof(inode), 0, inode_block);
        *st = inode.stat;

        return 0;
    }
    catch (bsnfs_error_t & error)
    {
        OUTPUT_ERROR(error);
        return -error.my_errno();
    }
    catch (...)
    {
        return -1;
    }
}

struct fuse_operations operations =
{
    .getattr    = bsnfs_do_getattr,
    .readdir    = bsnfs_do_readdir,
};
