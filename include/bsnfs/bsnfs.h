#ifndef BLOCKSNET_FUSE_BSNFS_H
#define BLOCKSNET_FUSE_BSNFS_H

#include <vector>
#include <unordered_map>
#include <cstdint>
#include <fstream>
#include <tools/arch/bsnfs_entry.h>
#include <sys/stat.h>

#define BLOCKSNET_FILESYSTEM_MAGIC_NUMBER 0x27EF8CD56302A7E3ULL
typedef uint64_t bsnfs_magic_t;
typedef uint64_t bsnfs_size_t;
typedef uint64_t bsnfs_time_t;
typedef struct stat bsnfs_stat_t;
typedef uint64_t bsnfs_block_t;

#define BSNFS_MODE_SHUTDOWN     0x00
#define BSNFS_MODE_OPERATIONAL  0x01
#define BSNFS_MODE_MAINTENANCE  0x02

/// Filesystem head
struct bsnfs_head_t
{
    bsnfs_magic_t   magic;              // Filesystem magic number
    bsnfs_size_t    fs_block_size;      // Filesystem block size
    bsnfs_size_t    fs_block_count;     // Filesystem block count
    bsnfs_time_t    last_cmount_time;   // Last mount time
    uint64_t        if_mounted:1,       // If filesystem is mounted
                    fs_current_mode:3;  // Current filesystem mode
    bsnfs_magic_t   magic_comp;         // Complement of filesystem magic number
};

/// Directory head (256 bit)
struct bsnfs_dir_head_t
{
    bsnfs_stat_t    stat;               // dir stat
    bsnfs_size_t    dir_block_count;    // Directory block count (yeah because I'm lazy)
    char __dummy__ [256 - sizeof (dir_block_count) - sizeof(stat)];
};

/// Directory tail (256 bit)
struct bsnfs_dir_tail_t
{
    bsnfs_size_t    next_dir_entry_num; // Directory entry count
    char __dummy__ [256 - sizeof (bsnfs_size_t)];
};

/// Directory entry (256 bit)
struct bsnfs_dentry_t
{
    bsnfs_block_t   dentry_block_num;   // Dentry block number
    char            entry_name          // Entry name
    [256 - sizeof (bsnfs_size_t)];
};

/// Index node (4096 bit)
struct bsnfs_inode_t
{
    bsnfs_stat_t    stat;               // File stat
    bsnfs_block_t   block_map[(4096 - sizeof (stat))/8]; // Block map
};

class bsnfs_block_map_t
{
private:
    std::fstream * device;              // raw device
    bsnfs_head_t fs_head { };           // filesystem head

public:
    /** @param _device opened device stream **/
    explicit bsnfs_block_map_t(const std::fstream & _device);

    /// read from device
    /** @param buffer read buffer
     *  @param block_num block number **/
    void read(char * buffer, bsnfs_block_t block_num);

    /// write to device
    /** @param buffer write buffer
     *  @param block_num block number **/
    void write(char * buffer, bsnfs_block_t block_num);

    /// read from device, return the actual read size (No bigger than one block)
    /** @param buffer read buffer
     *  @param size buffer size
     *  @param offset read offset
     *  @param block_num block number **/
    bsnfs_size_t read(char * buffer, bsnfs_size_t size, bsnfs_size_t offset, bsnfs_block_t block_num);

    /// write to device, return the actual write size (No bigger than one block)
    /** @param buffer write buffer
     *  @param size buffer size
     *  @param offset write offset
     *  @param block_num block number **/
    bsnfs_size_t write(char * buffer, bsnfs_size_t size, bsnfs_size_t offset, bsnfs_block_t block_num);
};

namespace bsnfs
{
    /// find dentry in dir block, throw BSNFS_NO_SUCH_FILE_OR_DIRECTORY if not found
    /** @param name target name
     *  @param dir_block directory block number
     *  @param block_map filesystem block map **/
    bsnfs_block_t find_in_dir(const char * name, bsnfs_block_t dir_block, bsnfs_block_map_t & block_map);

    /// return inode/dir head block number by pathname, throw BSNFS_NO_SUCH_FILE_OR_DIRECTORY if not found
    /** @param pathname pathname to file/dir
     *  @param block_map filesystem block map **/
    bsnfs_block_t nami(const bsnfs_pathname_t & pathname,
                       bsnfs_block_map_t & block_map);
}

#endif //BLOCKSNET_FUSE_BSNFS_H
