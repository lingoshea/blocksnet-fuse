#define FUSE_USE_VERSION 31

#include <fuse.h>
#include <iostream>
#include <tools/arch/do_fuse.h>
#include <cstring>
#include <fstream>
#include <sys/resource.h>
#include <tools/arch/bsnfs_error.h>

static std::fstream device_file;

enum {
    KEY_VERSION,
    KEY_HELP,
};

/// output version information
/** @param prog_name program name, given by *argv **/
void version(const char* prog_name);

/// output help information
/** @param prog_name program name, given by *argv **/
void help(const char* prog_name);

const char * device_path = nullptr;

static int opt_proc(void *data, const char *arg, int key,
                          struct fuse_args *outargs)
{
    static struct fuse_operations ss_nullptr = { };

    switch (key)
    {
        case FUSE_OPT_KEY_NONOPT:
            if (device_path == nullptr)
            {
                device_path = strdup(arg);
                std::cout << "Device path set to " << device_path << std::endl;
                device_file.open(device_path, std::ios::binary | std::ios::in | std::ios::out);

                if (!device_file)
                {
                    std::cerr << "Error when opening device `" << device_path
                              << "' (errno=" << strerror(errno) << ")" << std::endl;
                    exit(EXIT_FAILURE);
                }
                block_map = new bsnfs_block_map_t(device_file);
                return 0;
            }
            return 1;

        case KEY_VERSION:
            version(outargs->argv[0]);
            fuse_opt_add_arg(outargs, "--version");
            fuse_main(outargs->argc, outargs->argv, &ss_nullptr, nullptr);
            exit(EXIT_SUCCESS);

        case KEY_HELP:
            help(outargs->argv[0]);
            fuse_opt_add_arg(outargs, "-ho");
            fuse_main(outargs->argc, outargs->argv, &ss_nullptr, nullptr);
            exit(EXIT_SUCCESS);

        default:
            return 1;
    }
}

static struct fuse_opt opts[] = {
        FUSE_OPT_KEY("-V",        KEY_VERSION),
        FUSE_OPT_KEY("--version", KEY_VERSION),
        FUSE_OPT_KEY("-h",        KEY_HELP),
        FUSE_OPT_KEY("--help",    KEY_HELP),
        FUSE_OPT_END,
};

int main(int argc, char ** argv)
{
    try {
        struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

        if (fuse_opt_parse(&args, nullptr, opts, opt_proc) == -1) {
            std::cerr << "Couldn't parse the argument" << std::endl;
            return 1;
        }

        // increase stack size
        const rlim_t kStackSize = 32 * 1024 * 1024;
        rlimit rl{};
        int result;

        result = getrlimit(RLIMIT_STACK, &rl);
        if (result == 0) {
            if (rl.rlim_cur < kStackSize) {
                rl.rlim_cur = kStackSize;
                result = setrlimit(RLIMIT_STACK, &rl);
                if (result != 0) {
                    std::cerr << "setrlimit returned result=" << result << std::endl;
                }
            }
        }

        /*
         * d: enable debugging
         * s: run single threaded
         * f: stay in foreground
         */
        fuse_opt_add_arg(&args, "-s");
        fuse_opt_add_arg(&args, "-d"); // TODO: delete this
        fuse_opt_add_arg(&args, "-f"); // TODO: delete this
        int ret = fuse_main(args.argc, args.argv, &operations, nullptr);

        fuse_opt_free_args(&args);
        delete block_map;

        return ret;
    }
    catch (bsnfs_error_t & err)
    {
        std::cerr << "FATAL: " << err.what() << " (errno=" << err.what_errno() << ")" << std::endl;
        return 1;
    }
    catch (std::exception & err)
    {
        std::cerr << "System exception: " << err.what() << " (cur errno=" << strerror(errno) << ")" << std::endl;
    }
    catch (...)
    {
        std::cerr << "Unknown error caught! Terminating mount thread ..." << std::endl;
    }
}

void version(const char* prog_name)
{
    printf("%s Block-Net Snapshot Filesystem Mount Tool version 0.0.1\n", prog_name);
}

void help(const char *prog_name)
{
    version(prog_name);
    printf("usage: %s [device] [mountpoint] [options]\n"
            "\n"
            "general options:\n"
            "    -o opt,[opt...]        Mount options\n"
            "    -h   --help            Print help\n"
            "    -V   --version         Print version\n"
            "\n", prog_name);
}
