#ifndef BLOCKSNET_FUSE_BSNFS_ENTRY_H
#define BLOCKSNET_FUSE_BSNFS_ENTRY_H

#include <string>
#include <vector>

typedef std::vector<std::string> pathname_t;

/// BSNFS Entry
class bsnfs_pathname_t
{
private:
    pathname_t pathname;

public:
    /// create an entry link
    /// @param pathname entry
    explicit bsnfs_pathname_t(std::string pathname);

    [[nodiscard]] pathname_t get_pathname() const { return pathname; }
};


#endif //BLOCKSNET_FUSE_BSNFS_ENTRY_H
