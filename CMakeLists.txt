cmake_minimum_required(VERSION 3.20)
project(blocksnet_fuse)

set(CMAKE_CXX_STANDARD 17)

set(TOOLS_SOURCE_FILES
        tools/arch/bsnfs_entry.cpp      include/tools/arch/bsnfs_entry.h
        tools/arch/bsnfs_error.cpp      include/tools/arch/bsnfs_error.h
        tools/arch/do_fuse.cpp          include/tools/arch/do_fuse.h
        tools/data/xxhash64.cpp         include/tools/data/xxhash64.h
        bsnfs/bsnfs.cpp                 include/bsnfs/bsnfs.h
        )

set(LIBBNSNFS_SOURCE_FILES ${TOOLS_SOURCE_FILES})

add_library(bnsnfs STATIC ${LIBBNSNFS_SOURCE_FILES})
target_include_directories(bnsnfs PUBLIC include)
target_compile_options(bnsnfs PUBLIC "-D_FILE_OFFSET_BITS=64")
target_link_libraries(bnsnfs PUBLIC fuse)

# Mount executable
add_executable(mount.bsnfs tools/exec/mount.cpp)
target_link_libraries(mount.bsnfs PUBLIC bnsnfs)
target_include_directories(mount.bsnfs PUBLIC include)

# Format Filesystem executable
add_executable(mkfs.bsnfs tools/exec/mkfs.cpp)
target_link_libraries(mkfs.bsnfs PUBLIC bnsnfs)
target_include_directories(mkfs.bsnfs PUBLIC include)
